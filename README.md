# Introduction

This chart bootstraps a MediaWiki deployment on a Kubernetes cluster using the Helm package manager. It also packages the MySQL chart which is required for bootstrapping a MYSQL deployment for the database requirements of the MediaWiki application.

# Prerequisites
Kubernetes 1.12+
Tested on Helm 3.0+
PV provisioner support in the underlying infrastructure
ReadWriteMany volumes for deployment scaling

# Installing the Chart
To install the chart with the release name my-release:

$ helm install my-release charts/mediawiki

The command deploys MediaWiki on the Kubernetes cluster in the default configuration. The Parameters section lists the parameters that can be configured during installation.

# Uninstalling the Chart
To uninstall/delete the my-release deployment:

$ helm delete my-release
The command removes all the Kubernetes components associated with the chart and deletes the release.


# Reference
* [Bitnami-Wikipedia](https://github.com/helm/charts/tree/master/stable/mediawiki)

### Todos

 - Implemenet HPA for mediawiki pod so that the pod autoscales.
 - Move mysql to aws RDS for HA and better state management.
 - Document configuration parameters.
 - Multiple release files as per environment.
 - Jenkins file to deploy to multi environments.
 - Secrets are only base64 encoded, can use aws-kms to encrypt them or use hasgicorp vault.
 - Write tests
 - Implement Ingress




